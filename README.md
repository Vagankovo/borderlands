# Borderlands

Borderlands is a Minecraft plugin written by 14mRh4X0r for Sponge. It allows the user to create circlular, potentially overlapping borders, which the plugin prevents players exiting from.

## Installation

The prerequisite to using Borderlands is having Sponge installed on your server. Sponge's documentation (https://docs.spongepowered.org/stable/en/server/index.html) has the instructions on that. First build Borderlands' artifact file and then put it into your ``mods`` folder of your server. When first launched, it will generate a ``borderlands`` folder in your ``config`` folder.

## Usage

By default there are no Borders in a world, to create one you can do ``/border create <border-name> <radius> <x> <y> <z>``. Borders are potentially overlapping cylinders that are infinite in height.

Borderlands prevents players from exiting Borders on foot, on minecarts and boats, on mounts and through exiting a portal from the Nether.

If a player's last valid position is unknown, then they are sent to the nearest Border.

Players receive a two types of messages depending on whether their last valid position is known or not. You can edit them in the ``borderlands.conf`` configuration file.

Border data is stored in the ``borders.conf`` configuration file.

## Commands

Pipes are equivalent to "or".

- **/border** ...
    - Root command for Borderlands. Displays the command list when executed alone.
    - **add | create** <**name**> <**radius**> <**x**> <**y**> <**z**>
        - Creates a border with the given name and radius, whose center is at the given X and Z coordinates.
        - Y coordinate is unused, Borders are infinite in height.
    - **remove** <**name**>
        - Removes the border with the given name
    - **list**
        - Lists the borders in the server
    - **info**
        - Displays Borderlands version information
    - **reload**
        - Reloads the Borders from the config file

### Argument Syntax

| Notation | Meaning |
| :------: | :-----: |
| <...> | Required |

### Permissions
| Permission | Usage |
| :--------: | :---: |
|``com.minecraftonline.borderlands`` | Allows the user to use all Borderlands commands|
|``com.minecraftonline.borderlands.add`` | Allows the user to add Borders to the database|
|``com.minecraftonline.borderlands.remove`` | Allows the user to remove Borders from the database|
|``com.minecraftonline.borderlands.list`` | Allows the user to view the list of all Borders|
|``com.minecraftonline.borderlands.reload`` | Allows the user to reload Borderlands|


## License

This project is licensed under the GNU Affero GPL. You should have a copy of it together with this project. If not, see https://www.gnu.org/licenses/.

## Credits

14mRh4X0r (Willem Mulder) - Creator

Vagankovo - Contributor
