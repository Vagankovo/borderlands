/*
 * Copyright (C) 2017, 2019 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.borderlands;

import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.entity.RideEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Handles player movement.
 * @author Willem Mulder
 */
public class PlayerListener {
    private final Borderlands plugin;
    private final Borderlands.Settings settings;
    private final Map<Player, Transform<World>> lastKnownGoodMap;
    private final Map<Player, Long> lastNotifiedMap;

    public PlayerListener(Borderlands plugin) {
        this.plugin = plugin;
        this.settings = plugin.getSettings();
        this.lastKnownGoodMap = new WeakHashMap<>();
        this.lastNotifiedMap = new WeakHashMap<>();
    }

    private void sendMessage(Set<Player> players, Text message) {
        long currentTime = System.currentTimeMillis();
        players.forEach(player -> {
            Long lastNotified = lastNotifiedMap.get(player);
            if (lastNotified != null && currentTime - lastNotified < settings.getNotificationInterval()) {
                return;
            }

            player.sendMessage(message);
            lastNotifiedMap.put(player, currentTime);
        });
    }

    private Optional<Transform<World>> checkAndFixPosition(Entity toMove, Set<Player> players, Transform<World> src, Transform<World> dest) {
        if (plugin.isWithinAnyBorder(dest)) {
            return Optional.empty();
        }

        plugin.getLogger().debug("{} passed a border! (src = {}, dest = {})", players, src, dest);
        if (src.isValid() && plugin.isWithinAnyBorder(src)) {
            sendMessage(players, settings.getBorderMessage());
            return Optional.of(src);
        }

        // Source position is also invalid? Try the last known good position.
        plugin.getLogger().debug("{} didn't come from within a border!", players);
        Optional<Transform<World>> lastKnownGood = players.stream()
                .map(lastKnownGoodMap::get)
                .filter(Objects::nonNull)
                .filter(Transform::isValid)
                .filter(plugin::isWithinAnyBorder)
                .findAny();
        if (lastKnownGood.isPresent()) {
            sendMessage(players, settings.getBorderMessage());
            return lastKnownGood;
        } else { // when would this even happen? On login after changed borders.
            sendMessage(players, settings.getStrandedMessage());
            toMove.setLocationSafely(plugin.findNearestBorder(dest, 5).getLocation());
            return Optional.of(toMove.getTransform());
        }
    }

    @Listener
    public void handleMovement(MoveEntityEvent event, @First Entity entity) {
        Set<Player> players = getAllPlayerPassengers(entity);
        if (players.isEmpty()) return;

        // Update position and cross-world teleport packets crossed, ignore
        if (event.getFromTransform().getExtent() != entity.getWorld()) return;

        checkAndFixPosition(entity.getBaseVehicle(), players, event.getFromTransform(), event.getToTransform())
                .ifPresent(event::setToTransform);

        if (event.getFromTransform() == event.getToTransform()) {
            // If we stopped the thing in its tracks, set velocity to 0.
            entity.getBaseVehicle().setVelocity(new Vector3d());
            // Vehicle move events disregard toTransform, so cancel the event as well
            event.setCancelled(true);
        } else if (event.getFromTransform().getExtent() != event.getToTransform().getExtent()) {
            // Don't notify players directly after a cross-dimension teleport,
            // there may be position update packets from the old dimension left on the wire
            players.forEach(player -> lastNotifiedMap.put(player, System.currentTimeMillis()));
        }

        players.forEach(player -> lastKnownGoodMap.put(player, event.getToTransform()));
    }

    @Listener
    public void handleMount(RideEntityEvent.Mount event, @First Player player) {
        if (!plugin.isWithinAnyBorder(event.getTargetEntity().getLocation())) {
            event.setCancelled(true);
        }
    }

    private static Set<Player> getAllPlayerPassengers(Entity target) {
        if (target instanceof Player) return Collections.singleton((Player) target);
        return target.getPassengers().stream()
                .map(PlayerListener::getAllPlayerPassengers)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }
}
